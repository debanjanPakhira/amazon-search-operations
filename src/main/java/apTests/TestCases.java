
package apTests;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
//Selenium Imports
import java.util.logging.Level;

import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;

import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
///

public class TestCases {
    WebDriver driver;

    public TestCases() {
        System.out.println("Constructor: TestCases");

        WebDriverManager.chromedriver().timeout(30).setup();
        ChromeOptions options = new ChromeOptions();
        LoggingPreferences logs = new LoggingPreferences();

        // Set log level and type
        logs.enable(LogType.BROWSER, Level.ALL);
        logs.enable(LogType.DRIVER, Level.ALL);
        options.setCapability("goog:loggingPrefs", logs);

        // Set path for log file
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium Webdriver\\chromedriver.exe");

        driver = new ChromeDriver(options);

        // Set browser to maximize and wait
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    public void endTest() {
        System.out.println("End Test: TestCases");
        driver.close();
        driver.quit();

    }

    public void testCase01() throws InterruptedException {
        System.out.println("Start Test case: testCase01");
        driver.get("https://www.amazon.in/");
        String URL = driver.getCurrentUrl();

        Thread.sleep(2000);
        if(URL.contains("amazon")){
            System.out.println("the url contains to the expected title");
        }
        System.out.println("end Test case: testCase01");
    }

    public void testCase02() throws InterruptedException {

        System.out.println("Start Test case: testCase02");
        driver.get("https://www.amazon.in/");
        WebElement SearchBar = driver.findElement(By.id("twotabsearchtextbox"));
        SearchBar.sendKeys("laptop");
        SearchBar.sendKeys(Keys.ENTER);
        WebElement results = driver.findElement(By.xpath("//span[text()='Results']"));
        if(results.isDisplayed()){
            System.out.println("The Result text is displayed");

        }else
            System.out.println("No Result text is displayed");

        List<WebElement> items = driver.findElements(By.xpath("//*[@id=\"search\"]/div[1]/div[1]/div/span[1]/div[1]/div/div/div/div/div/div/div/div/div[2]/div/div/div[1]/h2"));
        int i=0;
        for(WebElement item: items){
            if(item.getText().contains("Laptop"))
                i++;
        }

        if(i>0){
            System.out.println("The resulting page contains the search term in at least one of the product titles or descriptions");
        }else
            System.out.println("Not resulting product is found");
        System.out.println("end Test case: testCase02");
    }

    public void testCase03() throws InterruptedException {
        System.out.println("Start Test case: testCase03");
        driver.get("https://www.amazon.in");
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("document.body.style.zoom = '0.6'");
        Thread.sleep(4000);

        WebElement electronics = driver.findElement(By.xpath("//*[@id='nav-xshop']/a[9]"));
        if(electronics.isEnabled()){
            System.out.println("Electronics button is enabled");

        }


        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].click();", electronics);


        String url = driver.getCurrentUrl();
        if(url.contains("electronics")){
            System.out.println("the resulting page corresponds to the clicked category");
        }
        System.out.println("end Test case: testCase03");
    }



}

